const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mysqlDb: {
        host     : 'localhost',
        user     : 'root',
        password : 'root1234',
        database: 'news2'
    }
};