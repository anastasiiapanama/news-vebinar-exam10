const express = require('express');
const router = express.Router();

const mysqlDb = require('../mysqlDb');

router.get('/', async (req, res) => {
    try {
        const postId = req.query.postId;
        console.log('req.query.newsId', postId);
        const connection = mysqlDb.getConnection();

        let query = 'SELECT * FROM COMMENTS';
        let params = [];

        if (postId) {
            query += ' WHERE post_id = ?';
            params = [postId];
            console.log(params)
        }

        const [comments] = await connection.query(query, params);

        return res.send(comments);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    try {
        const connection = mysqlDb.getConnection();

        const [result] = await connection.query('INSERT INTO comments (post_id, author, text) VALUES (?, ?, ?)',
            [req.body.postId, req.body.author, req.body.text]);

        console.log(result)

        return res.send({message: 'OK', id: result.insertId});
    } catch (e) {
        res.status(400).send(e );
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const connection = mysqlDb.getConnection();
        await connection.query('DELETE FROM comments WHERE id = ?', [req.params.id]);

        return res.send({message: 'DELETED', id: req.params.id});
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;