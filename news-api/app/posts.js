const path = require('path');
const express = require('express');
const router = express.Router();
const multer = require('multer');
const {nanoid} = require('nanoid');

const config = require('../config');
const mysqlDb = require('../mysqlDb');

const storage = multer.diskStorage({
    destination: (req, file, cb) => { // cb = callback
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const connection = mysqlDb.getConnection();
        const [posts] = await connection.query('SELECT id, title, image, created_at FROM posts');

        res.send(posts);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
   try {
       const connection = mysqlDb.getConnection();

       const image = req.file ? req.file.filename : null;

       if(!req.body.title || !req.body.content) {
           return res.status(400).send({error: 'Title or content is not present in request'});
       }

       const result = await connection.query(
           'INSERT INTO posts (title, content, image) VALUES (?, ?, ?)',
           [req.body.title, req.body.content, image]
       );

       res.send()
   } catch (e) {
       res.sendStatus(500);
   }
});

router.get('/:id', async (req, res) => {
    try {
        const connection = mysqlDb.getConnection();
        const [post] = await connection.query('SELECT * FROM posts WHERE id = ?', [req.params.id]);

        res.send(post[0]);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const connection = mysqlDb.getConnection();
        await connection.query('DELETE FROM posts WHERE id = ?', [req.params.id]);

        res.send()
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router; 