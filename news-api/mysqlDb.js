const mysql = require('mysql2/promise');
const config = require('./config');

let connection = null;

module.exports = {
    connect: async () => {
        connection = await mysql.createConnection(config.mysqlDb);

        console.log('Connection to MySQL successful! id=' + connection.threadId);
    },
    disconnect: async () => {
      await connection.close()
    },
    getConnection: () => connection
};