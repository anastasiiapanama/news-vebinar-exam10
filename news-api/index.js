const express = require('express');
const cors = require("cors");
const exitHook = require('async-exit-hook');

const mysqlDb = require('./mysqlDb');

const posts = require('./app/posts');
const comments = require('./app/comments');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/posts', posts);
app.use('/comments', comments);

const run = async () => {
    await mysqlDb.connect();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mysqlDb.disconnect();
        console.log('database disconnected');
        callback();
    });
};

run().catch(console.error);