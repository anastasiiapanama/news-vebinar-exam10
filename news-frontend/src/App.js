import React from 'react';
import {Route, Switch} from "react-router-dom";
import {Container, CssBaseline} from "@material-ui/core";
import AppToolbar from "./component/UI/AppToolbar/AppToolbar";
import Posts from "./containers/Posts/Posts";
import SinglePost from "./containers/SinglePost/SinglePost";

const App = () => (
    <>
      <CssBaseline/>
      <header>
        <AppToolbar/>
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={Posts}/>
            <Route path="/posts/:id" exact component={SinglePost}/>
          </Switch>
        </Container>
      </main>
    </>
);

export default App;
