import React, {useEffect, useState} from 'react';
import {fetchSinglePost} from "../../store/actions/postsActions";
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, Divider, Grid, Paper, TextField, Typography} from "@material-ui/core";
import {createComment, deleteComment, fetchCommentsForPost} from "../../store/actions/commentsActions";

const SinglePost = ({match}) => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.posts.singlePost);
    const commentsForPost = useSelector(state => state.comments.commentsForPost);
    const [author, setAuthor] = useState('');
    const [text, setText] = useState('');

    useEffect(() => {
        dispatch(fetchSinglePost(match.params.id));
        dispatch(fetchCommentsForPost(match.params.id));
    }, [dispatch, match.params.id]);

    const onCommentFormSubmit = async e => {
        e.preventDefault();

        await dispatch(createComment({
            author,
            text,
            postId: match.params.id
        }));

        setAuthor('');
        setText('');
    };

    return post ? (
        <Grid container direction="column" spacing="2">
            <Grid item xs>
                <Typography variant="h5">{post.title}</Typography>
            </Grid>
            <Grid item xs>
                <Typography variant="body2">{post.created_at}</Typography>
            </Grid>
            {post.image && (
                <Grid item xs>
                    <img src={'http://localhost:8000/uploads/' + post.image}
                         alt={post.title} style={{maxWidth: 200, maxHeight: 200}}
                    />
                </Grid>
            )}
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography variant="body1">{post.content}</Typography>
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid item xs>
                <Typography>Comments</Typography>
            </Grid>
            <Grid item xs container direction="column" spacing={1}>
                {commentsForPost.length === 0 ? (
                    <Typography variant={"body2"}>No comment</Typography>
                ) : commentsForPost.map(comment => (
                    <Grid item key={comment.id}>
                        <Paper component={Box}>
                            <Typography><b>Author:</b>{comment.author}</Typography>
                            <Typography>{comment.text}</Typography>
                        </Paper>
                        <Button onClick={() => dispatch(deleteComment(comment.id))}>Delete comment</Button>
                    </Grid>
                ))}
            </Grid>
            <Grid item xs>
                <Divider/>
            </Grid>
            <Grid container direction="column" spacing={2} component="form" onSubmit={onCommentFormSubmit}>
                <Grid item xs>
                    <Typography variant="h6">Add new comment</Typography>
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Author"
                        value={author}
                        onChange={e => setAuthor(e.target.value)}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Text"
                        value={text}
                        onChange={e => setText(e.target.value)}
                    />
                </Grid>
                <Grid item xs>
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Send comment
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    ) : null;
};

export default SinglePost;