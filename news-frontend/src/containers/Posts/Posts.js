import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts} from "../../store/actions/postsActions";
import {Grid, Paper, Box, Button} from "@material-ui/core";
import {Link} from "react-router-dom";

const Posts = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts.posts);

    useEffect(() => {
       dispatch(fetchPosts());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            {posts.map(post => (
                <Grid item xs key={post.id}>
                    <Paper component={Box} p={2}>
                        <Grid container spacing={2} alignItems="center">
                            {post.image && (
                                <Grid item>
                                    <img src={'http://localhost:8000/uploads/' + post.image}
                                         alt={post.title} style={{maxWidth: 200, maxHeight: 200}}
                                    />
                                </Grid>
                            )}
                                <Grid item>
                                    {post.title} ({post.created_at})
                                    <Button component={Link} to={"/posts/" + post.id}>View</Button>
                                </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            ))}
        </Grid>
    );
};

export default Posts;