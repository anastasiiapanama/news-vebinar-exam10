import {DELETE_COMMENT_SUCCESS, FETCH_COMMENTS_FOR_POST_SUCCESS} from "../actions/commentsActions";

const initialState = {
    commentsForPost: []
};

const commentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_FOR_POST_SUCCESS:
            return {...state, commentsForPost: action.comments};
        case DELETE_COMMENT_SUCCESS:
            return {
                ...state,
                commentsForPost: state.commentsForPost.filter(c => c.id !== action.id)
            };
        default:
            return state;
    }
};

export default commentsReducer;