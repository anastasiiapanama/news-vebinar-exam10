import axiosApi from "../../axios-api";

export const FETCH_COMMENTS_FOR_POST_SUCCESS = 'FETCH_COMMENTS_FOR_SUCCESS';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';

export const fetchCommentsForPostSuccess = comments => ({type: FETCH_COMMENTS_FOR_POST_SUCCESS, comments});
export const deleteCommentSuccess =  id => ({type: DELETE_COMMENT_SUCCESS, id});

export const createComment = commentData => {
    return async dispatch => {
        console.log('commentData', commentData);
        await axiosApi.post('/comments', commentData);
        await dispatch(fetchCommentsForPost(commentData.postId));
    }
};

export const fetchCommentsForPost = postId => {
    return async dispatch => {
        const response = await axiosApi.get('/comments?postId=' + postId);
        dispatch(fetchCommentsForPostSuccess(response.data));
    }
};

export const deleteComment = id => {
    return async dispatch => {
        await axiosApi.delete('/comments/' + id);
        dispatch(deleteCommentSuccess(id));
    }
};