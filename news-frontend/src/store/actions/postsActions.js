import axiosApi from "../../axios-api";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_SINGLE_POST_SUCCESS = 'FETCH_SINGLE_POST_SUCCESS';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchSinglePostSuccess = post => ({type: FETCH_SINGLE_POST_SUCCESS, post});

export const fetchPosts = () => {
  return async dispatch => {
      const response = await axiosApi.get('/posts');
      dispatch(fetchPostsSuccess(response.data));
  }
};

export const fetchSinglePost = id => {
  return async dispatch => {
      const response = await axiosApi.get('/posts/' + id);
      dispatch(fetchSinglePostSuccess(response.data));
  }
};